#include <stdio.h>

#include <omp.h>
#include <vector_types.h>
#include <vector_functions.h>

void BlurImageOpenMP(const uchar4 * const imageArray, 
					 uchar4 * const blurredImageArray, 
					 const long numRows, 
					 const long numCols, 
					 const float * const filter, 
					 const size_t filterWidth)
{
	using namespace std;

	const long halfWidth = filterWidth/2;

    #pragma omp parallel for collapse(2)
    for (long row = 0; row < numRows; ++row)
    {
        for (long col = 0; col < numCols; ++col)
        {
    	  float resR=0.0f, resG=0.0f, resB=0.0f;
		  for (long filterRow = -halfWidth; filterRow <= halfWidth; ++filterRow) 
		  {
		    for (long filterCol = -halfWidth; filterCol <= halfWidth; ++filterCol) 
		    {
		      //Find the global image position for this filter position
		      //clamp to boundary of the image
		      const long imageRow = min(max(row + filterRow, static_cast<long>(0)), numRows - 1);
		      const long imageCol = min(max(col + filterCol, static_cast<long>(0)), numCols - 1);

		      const uchar4 imagePixel = imageArray[imageRow*numCols+imageCol];
		      const float filterValue = filter[(filterRow+halfWidth)*filterWidth+filterCol+halfWidth];

		      resR += imagePixel.x*filterValue;
		      resG += imagePixel.y*filterValue;
		      resB += imagePixel.z*filterValue;
		    }
		  }

		  blurredImageArray[row*numCols+col] = make_uchar4(resR, resG, resB, 255);
        }
    }
}