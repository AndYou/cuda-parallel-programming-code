#include <iostream>

#include <cuda.h>

#include "CUDA_wrappers.hpp"
#include "common/CUDA_common.hpp"

__global__
void gaussian_blur(const uchar4* const d_image,
                   uchar4* const d_blurredImage,
                   const int numRows,
                   const int numCols,
                   const float * const d_filter, 
                   const int filterWidth)
{
  const int row = blockIdx.y*blockDim.y+threadIdx.y;
  const int col = blockIdx.x*blockDim.x+threadIdx.x;
    
  if (col >= numCols || row >= numRows)
    return;

  const int halfWidth = filterWidth/2;

  extern __shared__ float shared_filter[]; 
  if (threadIdx.y < filterWidth && threadIdx.x < filterWidth)
  {
    const int filterOff = threadIdx.y*filterWidth+threadIdx.x;
    shared_filter[filterOff] = d_filter[filterOff];  
  }
  __syncthreads();
    
  float resR=0.0f, resG=0.0f, resB=0.0f;
  for (int filterRow = -halfWidth; filterRow <= halfWidth; ++filterRow) 
  {
    for (int filterCol = -halfWidth; filterCol <= halfWidth; ++filterCol) 
    {
      //Find the global image position for this filter position
      //clamp to boundary of the image
      const int imageRow = min(max(row + filterRow, 0), numRows - 1);
      const int imageCol = min(max(col + filterCol, 0), numCols - 1);
      const uchar4 imagePixel = d_image[imageRow*numCols+imageCol];
      const float filterValue = shared_filter[(filterRow+halfWidth)*filterWidth+filterCol+halfWidth];
      resR += imagePixel.x * filterValue;
      resG += imagePixel.y * filterValue;
      resB += imagePixel.z * filterValue;        
    }
  }
  d_blurredImage[row*numCols+col] = make_uchar4(resR, resG, resB, 255);     
}

void BlurImageCUDA(const uchar4 * const h_image, 
                   uchar4 * const h_blurredImage, 
                   const size_t numRows, 
                   const size_t numCols, 
                   const float * const h_filter, 
                   const size_t filterWidth)
{
  uchar4 *d_image, *d_blurredImage;

  cudaSetDevice(0);
  checkCudaErrors(cudaGetLastError());

  const size_t numPixels = numRows * numCols;
  const size_t imageSize = sizeof(uchar4) * numPixels;
  //allocate memory on the device for both input and output
  checkCudaErrors(cudaMalloc(&d_image, imageSize));
  checkCudaErrors(cudaMalloc(&d_blurredImage, imageSize));

  //copy input array to the GPU
  checkCudaErrors(cudaMemcpy(d_image, h_image, imageSize, cudaMemcpyHostToDevice));

  float *d_filter;
  const size_t filterSize = sizeof(float) * filterWidth * filterWidth;
  checkCudaErrors(cudaMalloc(&d_filter, filterSize));
  checkCudaErrors(cudaMemcpy(d_filter, h_filter, filterSize, cudaMemcpyHostToDevice));

  dim3 blockSize;
  dim3 gridSize;
  int threadNum;

  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  threadNum = 32;
  blockSize = dim3(threadNum, threadNum, 1);
  gridSize = dim3(numCols/threadNum+1, numRows/threadNum+1, 1);
  cudaEventRecord(start);
  gaussian_blur<<<gridSize, blockSize, filterSize>>>(d_image, 
                                                     d_blurredImage, 
                                                     numRows, 
                                                     numCols, 
                                                     d_filter, 
                                                     filterWidth);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
  float milliseconds = 0;
  cudaEventElapsedTime(&milliseconds, start, stop);
  std::cout << "CUDA time kernel (ms): " << milliseconds << std::endl;

  checkCudaErrors(cudaMemcpy(h_blurredImage, d_blurredImage, sizeof(uchar4) * numPixels, cudaMemcpyDeviceToHost));

  checkCudaErrors(cudaFree(d_filter));
  checkCudaErrors(cudaFree(d_image));
  checkCudaErrors(cudaFree(d_blurredImage));
}

